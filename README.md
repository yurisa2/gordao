# GORDAO PROJECT #

Ok, so I got a Fitbit Charge HR and love cycling, just to find out they don't play together nicely. So I decided to give it a push...
IT WORKS!!! (it's on a !VERY! early development stage though)

### What this piece of software do? ###
It [connects](https://bitbucket.org/yurisa2/gordao/wiki/The%20process) to the Fitbit API to get your heartrate (Charge HR or Blaze) and writes it to your strava API (see wiki for details, Issues for limitations)
No DB, nothing on the server, just plain PHP.

### What this piece of software is? ###

* Based in the XGH philosophy (more at: https://gist.github.com/banaslee/4147370#file-xgh-en-txt )
* A good way to see pigs flying
* A simple connector between two services via browser.

### What this piece of software is **NOT**? ###

* Beautiful or well indented.
* Modern, in any way (other than the APIs themselves)
* An example for anything in the world

### Who am I? ###

[http://about.me/yuri.sa](http://about.me/yuri.sa)

Actually I'm coding a lot of [financial stuff](https://www.mql5.com/en/docs), so, my time is very limited.

But I do have my moments.

### What does it do? ###
Check the [wiki](https://bitbucket.org/yurisa2/gordao/wiki/browse/).

### What needs to be done ###
Check [Issues](https://bitbucket.org/yurisa2/gordao/issues?status=new&status=open).

### How to set up? ###
Check the [wiki](https://bitbucket.org/yurisa2/gordao/wiki/How%20to%20set%20up).

### How to ask something? ###
Check [Issues](https://bitbucket.org/yurisa2/gordao/issues/new).