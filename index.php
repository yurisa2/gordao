<?php
include 'files/sys/include.php';
session_start();

if(isset($_SESSION["fitbit_token"]))
{
// echo 'FitBit Token OK: '.$_SESSION["fitbit_token"].'<br>';
}
else
{
  fitbit_auth($fitbit_client_id);
}

if(isset($_SESSION["strava_token"]))
{
// echo 'Strava Token OK: '.$_SESSION["strava_token"].'<br>';
}
else
{
// header('Location: strava/strava_auth.php');
strava_auth($strava_client_id);
}

if(isset($_SESSION["fitbit_token"]) && $_SESSION["strava_token"]) header('Location: strava/strava_list_activities.php');

?>
