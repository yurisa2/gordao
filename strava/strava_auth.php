<?php
include '../files/sys/config.php';
include '../files/sys/include.php';


$return_link_strava = urldecode(substr('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'],0,-15)."strava_return.php");

$url ='https://www.strava.com/oauth/authorize?client_id='.$strava_client_id.'&response_type=code&redirect_uri='.$return_link_strava.'&scope=view_private,write&approval_prompt=auto';

redirect($url);

?>
