<?php
include '../files/sys/include.php';
//include '../files/sys/config.php';
session_start();


$file_name = $_GET['filename'];
$token_strava = $_SESSION["strava_token"];

$url_ret = 'https://www.strava.com/api/v3/activities/'.$file_name;
$activity_details = json_decode(create_query($url_ret,$token_strava),true);

$file = curl_file_create('../files/uploads/'.$file_name.'.gpx','multipart/mixed',$file_name.'.gpx'); // try adding

$headers = array();
$headers[] = 'Authorization: Bearer '.$token_strava;


$url_del = 'https://www.strava.com/api/v3/activities/'.$file_name;
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url_del);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

$stdel = curl_exec ($ch) or die(curl_error($ch));
//echo $result;
echo curl_error($ch);
curl_close($ch);



$post_fields = array(
    "activity_type" => "ride",
    "file" => $file,
    "data_type" => "gpx"
);

$url = "https://www.strava.com/api/v3/uploads";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_SAFE_UPLOAD,true);
curl_setopt($ch, CURLOPT_POSTFIELDS,$post_fields);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

$st = curl_exec($ch);
curl_close($ch);

$upload_response = json_decode($st,true);

$url = 'https://www.strava.com/api/v3/uploads/'.$upload_response['id'];
$upload_details = json_decode(create_query($url,$token_strava),true);

while($upload_details['activity_id']=="")
{

$upload_details = json_decode(create_query($url,$token_strava),true);
//print_r($upload_details);
//echo '<br>';

//echo 'New Activity ID:'.$upload_details[activity_id];

sleep(1);
}


$post_fields_change = array(
    "activity_type" => "ride",
    "commute" => $activity_details['commute'],
    "gear" => $activity_details['gear'],
    "name" => $activity_details['name'].' (Fitbit HR by @yurisa2)',
    "private" => $activity_details['private'],
    "description" => $activity_details['description'].' using gordao (https://bitbucket.org/yurisa2/gordao)! by about.me/yuri.sa'
);


$headers = array();
$headers[] = 'Authorization: Bearer '.$token_strava;

$url_new = 'https://www.strava.com/api/v3/activities/'.$upload_details['activity_id'];

//echo '<br>'.$url_new.'< = URL NOVA<BR>';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url_new);
curl_setopt($ch, CURLOPT_SAFE_UPLOAD,true);
curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($post_fields_change));
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); // note the PUT here
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$st = curl_exec($ch);
curl_close($ch);

//print_r(json_decode($st,true));  // $st[id]



//var_dump($st);

//if($delete_gpx_files==true)
//{
//unlink('../files/uploads/'.$file_name.'.gpx');
//}

echo '
      <div class="col-xs12">
        <div class="panel panel-default">
          <div class="panel-heading">
          Activity Updated Successfully!
          </div>
          <div class="panel-body">
            <div class="row">
              <a href="https://www.strava.com/activities/'.$upload_details['activity_id'].'" class="col-xs-12 col-md-6 btn btn-primary btn-block" target="_blank">View it on STRAVA!</a>
              <a href="../index.php" class="col-xs-12 col-md-6 btn btn-primary btn-block">Back to Listing</a>
            </div>
          </div>
        </div>
    </div>

        '
      ;

?>
