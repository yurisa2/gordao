<?php
include '../files/sys/config.php';
include '../files/sys/include.php';


session_start();

$url = "https://www.strava.com/oauth/token";

$post_fields = array();
$post_fields[client_id] = $strava_client_id;
$post_fields[client_secret] = $strava_client_secret;
$post_fields[code] = $_GET['code'];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_SAFE_UPLOAD,true);
curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($post_fields));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

$result = curl_exec ($ch) or die(curl_error($ch));
//echo $result;
echo curl_error($ch);
curl_close($ch);

$auth_strava = json_decode($result,true);

$_SESSION['strava_token'] = $auth_strava[access_token];
//var_dump($auth_strava);

//echo $_SESSION["fitbit_token"].'<br>';
//echo $_SESSION["strava_token"].'<br>';

 redirect('strava_list_activities.php');

// header('Location: index.php);
?>
