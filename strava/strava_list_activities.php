<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');

include '../fitbit/fitbit_lista_intraday.php';
include '../files/sys/include.php';
include '../files/sys/config.php';

session_start();

if(!isset($_SESSION['strava_token']) || !isset($_SESSION['fitbit_token'])) redirect('../index.php');

$token_strava = $_SESSION['strava_token'];
$token_fitbit = $_SESSION['fitbit_token'];

// echo 'strava - '.$_SESSION['strava_token'];
// echo '<br>fitbit SesS- '.$_SESSION['fitbit_token'];

//var_dump($_SESSION);
$_activities = json_decode(create_query("https://www.strava.com/api/v3/athlete/activities",$token_strava),true);

echo '
<div class="container">
<div class="row">
';

foreach ($_activities as $key => $value)
{
	$init_time = strtotime($value['start_date']);
	$elapsed_time = $value['elapsed_time'];
	$name = $value['name'];
	$kudos_count  = $value['kudos_count'];
	$is_commute = $value['commute'];
	$is_private = $value["private"];

	if(isset($value['average_heartrate'])) $avg_HR = $value['average_heartrate'];
	else $avg_HR = '0';

	if(isset($value['photos']['count'])) $photo_count = $value['photos']['count'];
	else $photo_count = '0';


if(round($elapsed_time) > '3600') $duration = gmdate('H:i',round($elapsed_time)).' h';
else {
	$duration = round($elapsed_time/'60').' min';

}

		$display_box = '
		<div class="panel panel-default col-sm-12 col-md-3 center-block">
		<div class="panel-heading no-vaz">
		<h3>'.$value['name'].'</h3>
		</div>
		<div class="panel-body">
		<i class="fa fa-calendar" aria-hidden="true"></i> '.date('Y-m-d',strtotime($value['start_date'])).' <i class="fa fa-clock-o" aria-hidden="true"></i> '.date('H:i',strtotime($value['start_date'])).' + '.$duration.'<br>';
		if(round($kudos_count) > 0)
		$display_box .= ' <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>' .round($kudos_count);
		if(round($is_commute) > 0)
		$display_box .= ' | <i class="fa fa-briefcase" aria-hidden="true"></i> ';
		if(round($is_private) > 0)
		$display_box .= ' | <i class="fa fa-lock" aria-hidden="true"></i> ';
		if(round($avg_HR) > 0)
		$display_box .= ' | <i class="fa fa-heart" aria-hidden="true"></i> '.round($avg_HR);
		if(round($photo_count) > 0)
		$display_box .= ' | <i class="fa fa-camera" aria-hidden="true"></i> '.round($photo_count);
		$display_box .= '<br>
		<img src="https://maps.googleapis.com/maps/api/staticmap?size=350x300&0&path=weight:3|color:red|enc:'.$value['map']['summary_polyline'].'&key='.$Google_APIs_Key.'" class="img-thumbnail center-block img-reponsive" alt="Ride MAP!">
		<a href="strava_activity_details.php?id='.$value['id'].'&init_time='.$init_time.'&elapsed_time='.$elapsed_time.'&name='.urlencode($name).'&strava_token='.$token_strava.'&fitbit_token='.$token_fitbit.'" class="col-xs-12 col-md-12 btn btn-warning">Execute!</a><br>
		<a href="https://www.strava.com/activities/'.$value['id'].'" class="col-xs-12 col-md-12 btn btn-primary " target="_blank">On STRAVA</a>
		</div>
		</div>';
		echo $display_box;
}     //end FOR GLOBAL

echo '

<br>
<div class="panel panel-default col-sm-12 col-md-3 center-block">
<div class="panel-heading no-vaz">
<h3><a href ="../files/sys/reset_session.php"> UNSET SESSION </a><br>
</h3>
</div>
<div class="panel-body">
We can forget about the connection details using this button, but if you keep logged on the strava or fitbit, your details will be sessioned again. Logout from the services first, if you really want to get out. Again, we don`t use any kind of persistent data storage (DB).
<br>

<br><a href ="https://www.strava.com/session" class="btn btn-primary"> STRAVA logout </a><br>
<br><a href ="https://www.fitbit.com/logout" class="btn btn-primary"> FITBIT logout </a><br>
<br><a href ="../files/sys/reset_session.php" class="btn btn-primary"> UNSET SESSION </a><br>

</div>
</div>

</div>
</div>

</body>
</html>
';


?>
