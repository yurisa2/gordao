<?php
session_start();

$fitbit_token = $_GET['fitbit_token'];
$id = $_GET['id'];
$init_time = $_GET['init_time'];
$elapsed_time = $_GET['elapsed_time'];
$name = $_GET['name'].' - HR by @yurisa2';
$strava_token = $_GET['strava_token'];

include '../fitbit/fitbit_lista_intraday.php' ;
include '../files/sys/include.php';
include '../files/sys/config.php';

//echo 'Getting Activity data <br>';

$streams = json_decode(create_query("https://www.strava.com/api/v3/activities/".$id."/streams/latlng",$strava_token),true);
$streams_alt = json_decode(create_query("https://www.strava.com/api/v3/activities/".$id."/streams/altitude",$strava_token),true);
$streams_time = json_decode(create_query("https://www.strava.com/api/v3/activities/".$id."/streams/time",$strava_token),true);

foreach ( $streams[0]['data'] as $key => $value)
{
  $_integr[] = array('lat' => $value['0'],'lng' => $value[1]);
}

foreach ( $streams_alt['1']['data'] as $key => $value)
{
  $_integr[$key]['ele'] = $value;
}

$hr_array = fitbit_hr($init_time,$elapsed_time,$fitbit_token);

foreach ( $streams_time['0']['data'] as $key => $value)
{
  $_integr[$key]['time'] = substr(gmdate('c',$value + $init_time), 0, -6).'Z';
  if(isset($hr_array[$value + $init_time])) $_integr[$key]['heartrate'] = $hr_array[$value + $init_time];
}

//echo 'Starting GPX - File <br>';

$gpx_output = '<?xml version="1.0" encoding="UTF-8"?>
<gpx creator="StravaGPX" version="1.1" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
<metadata>
<time>'.substr(gmdate('c',$init_time), 0, -6).'Z'.'</time>
</metadata>
<trk>
<name>'.$name.'</name>
<trkseg>';

foreach ($_integr as $key => $value)
{
  $gpx_output .= '<trkpt lat="'.$_integr[$key]['lat'].'" lon="'.$_integr[$key]['lng'].'">';
  $gpx_output .= '<ele>'.$_integr[$key]['ele'].'</ele>';
  $gpx_output .= '<time>'.$_integr[$key]['time'].'</time>';

  if(isset($_integr[$key]['heartrate']))
  $gpx_output .= '<extensions>\n<gpxtpx:TrackPointExtension>\n<gpxtpx:hr>'.$_integr[$key]['heartrate'].'</gpxtpx:hr></gpxtpx:TrackPointExtension></extensions>';
  $gpx_output .= '</trkpt>';
}

$gpx_output .= '</trkseg></trk></gpx>';

file_put_contents('../files/uploads/'.$id.'.gpx', $gpx_output);
//echo $gpx_output;

//echo 'Ending gpx File<br>';
      redirect('strava_upload.php?filename='.$id);

//echo "<a href=strava_upload.php?filename=$id&strava_token=$strava_token>Upload</a>";

?>
