<?php
include 'config.php';

function create_query($url,$bearer)
{
  //$url = "https://www.strava.com/api/v3/athlete/activities";   //Test it by hand

  echo '<!DOCTYPE html lang="pt">
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="../files/bootstrap/dist/css/bootstrap.min.css">

  <link rel="stylesheet" type="text/css" href="../files/sys/style.css">
  <script src="https://use.fontawesome.com/ee1f22cb26.js"></script>
  <title>Yuri Sa Gordao Fitness System</title>
  </head>
  <body>
  ';

  $headers = array();
  $headers[] = 'Authorization: Bearer '.$bearer;

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_SAFE_UPLOAD,true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

  $result = curl_exec ($ch) or die(curl_error($ch));
  //echo $result;
  echo curl_error($ch);
  curl_close($ch);

  return $result;
}

function fitbit_auth($fitbit_client_id)
{
  $return_link = urlencode('http://'.$_SERVER['HTTP_HOST'].'/gordao/fitbit/fitbit_return.php'); // Do it more dinamically

  //  $return_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

  header('Location: https://www.fitbit.com/oauth2/authorize?response_type=token&client_id='.$fitbit_client_id.'&redirect_uri='.$return_link.'&scope=heartrate%20location%20profile%20settings&expires_in=31536000');
  //  echo 'client id de dentro: '.$fitbit_client_id.'<br>';
  //  echo 'Location: https://www.fitbit.com/oauth2/authorize?response_type=token&client_id='.$fitbit_client_id.'&redirect_uri='.$return_link.'&scope=heartrate%20location%20profile%20settings&expires_in=31536000';
  //echo $return_link;

}

function strava_auth($strava_client_id)
{
  $return_link_strava = urldecode(substr('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'],0,-10)."/strava/strava_return.php");
  $url ='https://www.strava.com/oauth/authorize?client_id='.$strava_client_id.'&response_type=code&redirect_uri='.$return_link_strava.'&scope=view_private,write&approval_prompt=auto';
  header('Location: '.$url);

}


function fitbit_hr($date_ent, $date_duration,$fitbit_token)
{
global $fitbit_time_offset;
global $prim_hr;

$fitbit_time_offset_sec = (-1)*$fitbit_time_offset*60*60;

$date_ent_as_date  = strtotime($fitbit_time_offset.' hours',$date_ent);

$date_ent = substr(date('c' ,$date_ent_as_date), 0, -6).'Z';

$url =  "https://api.fitbit.com/1/user/-/activities/heart/date/".gmdate('Y-m-d',$date_ent_as_date)."/1d/1sec/time/".gmdate('H:i',$date_ent_as_date)."/".gmdate('H:i',$date_ent_as_date+$date_duration).".json";

$jd = json_decode(create_query($url,$fitbit_token),true);

        foreach ( $jd['activities-heart-intraday']['dataset'] as $key => $value)
            {
                $sec_time = strtotime(gmdate('Y-m-d',strtotime($date_ent)).'T'.$jd['activities-heart-intraday']['dataset'][$key]['time'].'Z');
                $prim_hr[$sec_time + $fitbit_time_offset_sec] = $jd['activities-heart-intraday']['dataset'][$key]['value'];   // OGREEEEEEEEE
            }

            $preench_hr = array_fill(min(array_keys($prim_hr)),max(array_keys($prim_hr))-min(array_keys($prim_hr)),"0");

        foreach ( $prim_hr as $key => $value)
            {
                $preench_hr[$key] = $value;
            }

        foreach ( $preench_hr as $key => $value)
            {
                if($preench_hr[$key] > 0) $last_hr = $preench_hr[$key];
                if($preench_hr[$key] == 0) $preench_hr[$key] = $last_hr;

            }

//var_dump($jd);

            return $preench_hr;

}

function redirect($url)
{
    if (!headers_sent())
    {
        header('Location: '.$url);
        exit;
        }
    else
        {
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>'; exit;
    }
}

?>
